//===-- UnionFind/QuickFind.h -----------------------------------*- C++ -*-===//
//
//                              Algorithm I
//
// This file is distributed under a BSD-like Open Source Licence.  See
// LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//


#ifndef QUICKFIND_H
#define QUICKFIND_H

#include <cstddef>
#include <vector>


class QuickFind
{
  std::vector<size_t> elements;
 public:
  QuickFind(size_t size);
  virtual ~QuickFind();

  void Union(size_t first, size_t second);

  bool Find(size_t first, size_t second);
};


#endif /* QUICKFIND_H */
