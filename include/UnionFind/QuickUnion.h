//===-- UnionFind/QuickUnion.h ----------------------------------*- C++ -*-===//
//
//                              Algorithm I
//
// This file is distributed under a BSD-like Open Source Licence.  See
// LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//


#ifndef QUICKUNION_H
#define QUICKUNION_H

#include <cstddef>
#include <vector>


class QuickUnion
{
  std::vector<size_t> elements;
  std::vector<size_t> sizes;

  size_t root(size_t E);

 public:
  QuickUnion(size_t size);
  virtual ~QuickUnion();

  void Union(size_t A, size_t B);
  bool Connected(size_t A, size_t B);
};

#endif /* QUICKUNION_H */
