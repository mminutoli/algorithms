//===-- UnionFind/QuickFind.cc ----------------------------------*- C++ -*-===//
//
//                              Algorithm I
//
// This file is distributed under a BSD-like Open Source Licence.  See
// LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//


#include <UnionFind/QuickFind.h>

#include <cassert>


QuickFind::QuickFind(size_t size)
    : elements(size)
{
  for (size_t i = 0; i < size; ++i) {
    elements[i] = i;
  }
}


QuickFind::~QuickFind() {}


void QuickFind::Union(size_t F, size_t S)
{
  assert(F < elements.size());
  assert(S < elements.size());
  size_t idF = elements[F];
  size_t idS = elements[S];
  for (std::vector<size_t>::iterator
           Itr = elements.begin(), End = elements.end(); Itr != End; ++Itr)
  {
    if (*Itr == idF) *Itr = idS;
  }
}


bool QuickFind::Find(size_t F, size_t S)
{
  return elements[F] == elements[S];
}
