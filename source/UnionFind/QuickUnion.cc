//===-- UnionFind/QuickUnion.cc ---------------------------------*- C++ -*-===//
//
//                              Algorithm I
//
// This file is distributed under a BSD-like Open Source Licence.  See
// LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//


#include <UnionFind/QuickUnion.h>

#include <cassert>


QuickUnion::QuickUnion(size_t size)
    : elements(size), sizes(size)
{
  for (size_t i = 0; i < size; ++i) {
    elements[i] = i;
    sizes[i] = 1;
  }
}


QuickUnion::~QuickUnion() {}


void QuickUnion::Union(size_t A, size_t B)
{
  assert(A < elements.size());
  assert(B < elements.size());

  if (A == B) return;

  size_t rootB = root(B);
  size_t rootA = root(A);
  if (sizes[rootB] > sizes[rootA])
  {
    elements[rootA] = rootB;
    sizes[rootB] += sizes[rootA];
  } else {
    elements[rootB] = rootA;
    sizes[rootA] += sizes[rootB];
  }
}


bool QuickUnion::Connected(size_t A, size_t B)
{
  assert(A < elements.size());
  assert(B < elements.size());
  return root(A) == root(B);
}


size_t QuickUnion::root(size_t E)
{
  assert(E < elements.size());
  while (E != elements[E]) {
    elements[E] = elements[elements[E]];
    E = elements[E];
  }
  return E;
}
