#include <UnionFind/QuickFind.h>
#include <gtest/gtest.h>

TEST(QuickFind, test0)
{
  QuickFind QF(10);

  QF.Union(0, 1);
  ASSERT_TRUE(QF.Find(0, 1));
  ASSERT_TRUE(QF.Find(1, 0));
  ASSERT_FALSE(QF.Find(2, 0));

  QF.Union(5, 6);
  QF.Union(8, 7);
  QF.Union(1, 7);

  ASSERT_FALSE(QF.Find(0, 5));
  ASSERT_TRUE(QF.Find(1, 7));
}
