#include "UnionFind/QuickUnion.h"
#include <gtest/gtest.h>

TEST(QuickUnion, test0)
{
  QuickUnion QU(10);

  QU.Union(0, 1);
  ASSERT_TRUE(QU.Connected(0, 1));
  ASSERT_TRUE(QU.Connected(1, 0));
  ASSERT_FALSE(QU.Connected(2, 0));

  QU.Union(5, 6);
  QU.Union(8, 7);
  QU.Union(1, 7);

  ASSERT_FALSE(QU.Connected(0, 5));
  ASSERT_TRUE(QU.Connected(1, 7));
}
